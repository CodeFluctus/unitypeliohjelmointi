﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls2 : MonoBehaviour
{

    public float hoverPower;

    public float hoverHeight;

    public float thrustSpeed;
    public float turnSpeed;

    public float spaceshipSpeed = 0.0f;

    private float thrustInput;
    private float turnInput;

    //Floats spaceship
    private bool antigravityEnginePowerOn = true;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //if(antigravityEnginePowerOn == true)
        //{
            thrustInput = Input.GetAxis("Vertical");
            turnInput = Input.GetAxis("Horizontal");
        //}
        //Switch engine off if it is on. And on if it is on
        if(Input.GetKeyUp(KeyCode.R))
        {
            antigravityEnginePowerOn = !antigravityEnginePowerOn;
            Debug.Log("Switched AntigravityEniginePower is on: " + antigravityEnginePowerOn);
        }
        
        /*if(Input.GetKeyUp(KeyCode.R) && !tiresTouchingGround())
        {
            flipCarBackOnTires();
        }
        if(Input.GetKeyUp(KeyCode.G))
        {
            showRotationRelativeToWorld();
        }*/
    }

    private void FixedUpdate() 
    {
        spaceshipSpeed = rb.velocity.magnitude;
        rb.AddRelativeForce(0f,0f, thrustInput * thrustSpeed);
        rb.AddRelativeTorque(0f, turnInput * turnSpeed, 0f);
        if(antigravityEnginePowerOn)
        {
            Debug.Log("TRYING Hovering spaceship!");
            // Hovering
            Vector3 rayStartPosition = new Vector3(transform.position.x, transform.position.x, transform.position.z);
            Ray ray = new Ray( /*rayStartPosition*/transform.position + Vector3.up *3.0f/* + new Vector3(transform.position.x, (transform.position.x + 2.0f), transform.position.z)*/, -transform.up);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, hoverHeight)) 
            {
                Debug.Log("ACTUALLY Hovering spaceship!");
                float proportionalHeight = (hoverHeight - hit.distance) / hoverHeight;

                Vector3 appliedHoverForce = Vector3.up * proportionalHeight * hoverPower;

                rb.AddForce(appliedHoverForce, ForceMode.Acceleration);
            }
        }
        
    }
}
