﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{

    public float thrustSpeed;
    public float turnSpeed;

    public float carSpeed = 0.0f;

    public float spaceBetweenCarAndGround = 5.0f;

    private float thrustInput;
    private float turnInput;
    private bool carFlippedOver = false;
    private Rigidbody carRigidBody;

    // Start is called before the first frame update
    void Start()
    {
        carRigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        thrustInput = Input.GetAxis("Vertical");
        turnInput = Input.GetAxis("Horizontal");
        if(Input.GetKeyUp(KeyCode.R) && !tiresTouchingGround())
        {
            flipCarBackOnTires();
        }
        if(Input.GetKeyUp(KeyCode.G))
        {
            showRotationRelativeToWorld();
        }
    }

    void FixedUpdate() {

        carSpeed = carRigidBody.velocity.magnitude;
        if(tiresTouchingGround())
        {
            carRigidBody.AddRelativeForce(0f,0f, thrustInput * thrustSpeed);
            //If car is moving forward or backward it is possible to turn car. Othervice it is impossible
            //Debug.Log("Car's speed is: " + carSpeed);
            if(carSpeed > 5)
            {
                carRigidBody.AddRelativeTorque(0f, turnInput * turnSpeed, 0f);
            }
            
        }

        /*Debug.Log("Car's x-position: " + carRigidBody.position.x);
        Debug.Log("Car's y-position: " + carRigidBody.position.y);
        Debug.Log("Car's z-position: " + carRigidBody.position.z);
        */
    }

    bool tiresTouchingGround()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, -transform.up);
        if(Physics.Raycast(ray, out hit, spaceBetweenCarAndGround))
        {
            Debug.Log("On ground!");
            //Debug.Log(hit.collider.tag);
            carFlippedOver = false;
            return true;
        }
        else
        {
            //Debug.Log("Airbone!");
            //Debug.Log(hit.collider.tag);
            carFlippedOver = true;
            /*if(hit == null)
            {
                carFlippedOver = true;
                Debug.Log("CAR FLIPPED OVER!");
            }*/
            return false;
        }
    }

    void flipCarBackOnTires()
    {
        float xPosition = carRigidBody.position.x;
        float yPosition = carRigidBody.position.y;
        float zPosition = carRigidBody.position.z;
        
        //Vector3 carPosition = new Vector3(xPosition, Vector3.up*3, zPosition);

        float yRotation = carRigidBody.rotation.y;

//carRigidBody.transform.rotation = new Quaternion(0, 0, yRotation, -90);
        //WORKING
        carRigidBody.transform.position = carRigidBody.transform.position + new Vector3(0, 3, 0);
        //carRigidBody.transform.Translate(Vector3.forward);// = carPosition;//new Vector3(xPosition, Vector3.up*3, zPosition);//carRigidBody.position.TransformPosition(xPosition, yPosition +3, zPosition);
        //WORKING
        //carRigidBody.transform.Rotate(0,yRotation, 180, Space.World);
        //carRigidBody.transform.Rotate(0.0f,yRotation, 0.0f, Space.World);
        carRigidBody.transform.rotation = new Quaternion(0.0f, yRotation, 0,0f);
        Debug.Log("Car flipped back on tires! Have Fun!");
    }

    void showRotationRelativeToWorld()
    {
        Debug.Log(carRigidBody.transform.rotation);
    }

}
