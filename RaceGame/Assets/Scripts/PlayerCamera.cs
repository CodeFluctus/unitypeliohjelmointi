﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{

    public GameObject playerCar;

    private float height = 8.0f;
    private float distance = -15.0f;

    private float followDamping = 8f;
    private float rotationDamping = 100.0f;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = playerCar.transform.TransformPoint(0f, height, distance);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /*
        transform.position = playerCar.transform.TransformPoint(0f, height, distance);
        transform.rotation = playerCar.transform.rotation;
        */

        transform.position = Vector3.Lerp(transform.position,
            playerCar.transform.TransformPoint(0f, height, distance),
            Time.deltaTime * followDamping);
        Quaternion rotation =
            Quaternion.LookRotation(playerCar.transform.position
            - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation,
            Time.deltaTime * rotationDamping);
    }
}
