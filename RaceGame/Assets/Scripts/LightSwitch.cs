﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : MonoBehaviour
{
    private bool lightsOn = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.H))
        {
            if(lightsOn)
            {
                this.GetComponent<Light>().enabled = false;
                lightsOn = false;
            }
            else
            {
                this.GetComponent<Light>().enabled = true;
                lightsOn = true;
            }
            
        }
    }
}
